MinecraftUtilities Plugin for Bukkit Server
============

Created by [Daniel Schneider](https://gitlab.com/Schneidoa)

Introduction
------------

Add some cool commands

Requirements
------------
 * Bukkit compatible server version 1.12.2 (spigot 1.12.2)


Installation
------------

### Main Setup

1. Download plugin jarfile (https://gitlab.com/Schneidoa/MinecraftUtilities)
2. Put plugin jarfile in "plugins" folder
3. Stop and start server or reload the config

## Usage

### Commands
#### /locktime \<lock> \<time>
Parameter:
1. lock: boolean (true|false)
2. time: Time as String (see config.yml [time.*]) or as Integer (Minecraft Ticks)


Example:

````
/locktime true day
/locktime true night
/locktime true 1000

/locktime false
````

#### /lockweather \<lock> \<storm> \<thunder>
Parameter:
1. lock: boolean (true|false)
1. storm: boolean (true|false) [Optional, default false]
1. thunder: boolean (true|false) [Optional, default false]


Example:

````
/lockweather true false false
/lockweather ture false
````
