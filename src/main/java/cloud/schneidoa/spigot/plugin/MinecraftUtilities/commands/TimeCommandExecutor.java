package cloud.schneidoa.spigot.plugin.MinecraftUtilities.commands;

import cloud.schneidoa.spigot.plugin.MinecraftUtilities.MinecraftUtilities;
import cloud.schneidoa.spigot.plugin.MinecraftUtilities.helpers.ConfigHelper;
import cloud.schneidoa.spigot.plugin.MinecraftUtilities.world.Time;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TimeCommandExecutor implements CommandExecutor {

    private final MinecraftUtilities plugin;

    public TimeCommandExecutor(MinecraftUtilities plugin) {
        this.plugin = plugin; // Store the plugin in situations where you need it.
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            if (command.getName().equalsIgnoreCase("locktime")) {
                if (strings.length > 2) {
                    commandSender.sendMessage("Too many arguments!");
                    return false;
                }
                if (strings.length < 1) {
                    commandSender.sendMessage("Not enough arguments!");
                    return false;
                }


                if (strings[0].equalsIgnoreCase("true")) {
                    if (strings.length != 2) {
                        commandSender.sendMessage("Not enough arguments!");
                        return false;
                    }


                    Long ticks;
                    try {
                        ticks = Long.valueOf(strings[1]);
                    } catch (NumberFormatException e) {
                        if (plugin.getConfig().isSet("time." + strings[1])) {
                            ticks = plugin.getConfig().getLong("time." + strings[1]);
                        } else {
                            commandSender.sendMessage("Invalid parameter: " + strings[1]);
                            return false;
                        }
                    }
                    Time.lockTime(plugin, ((Player) commandSender).getWorld(), true, ticks);

                    commandSender.sendMessage("The time circle is locked to " + ticks.toString());
                } else {
                    Time.lockTime(plugin, ((Player) commandSender).getWorld(), false, null);
                    commandSender.sendMessage("The time circle is unlocked");
                }


            }

        } else {
            commandSender.sendMessage(ConfigHelper.getString(plugin, "messages.error.onlyPlayers"));
        }

        return true;
    }
}
