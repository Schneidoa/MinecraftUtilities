package cloud.schneidoa.spigot.plugin.MinecraftUtilities.commands;

import cloud.schneidoa.spigot.plugin.MinecraftUtilities.MinecraftUtilities;
import cloud.schneidoa.spigot.plugin.MinecraftUtilities.helpers.ConfigHelper;

import cloud.schneidoa.spigot.plugin.MinecraftUtilities.world.Weather;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class WeatherCommandExecutor implements CommandExecutor {

    private final MinecraftUtilities plugin;

    public WeatherCommandExecutor(MinecraftUtilities plugin) {
        this.plugin = plugin; // Store the plugin in situations where you need it.
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            if (command.getName().equalsIgnoreCase("lockweather")) {
                if (strings.length > 3) {
                    commandSender.sendMessage("Too many arguments!");
                    return false;
                }
                if (strings.length < 1) {
                    commandSender.sendMessage("Not enough arguments!");
                    return false;
                }


                if (strings[0].equalsIgnoreCase("true")) {

                    Boolean strom = false;
                    if (strings.length > 1 && strings[1].equalsIgnoreCase("true")) {
                        strom = true;
                    }

                    Boolean thunder = false;
                    if (strings.length > 2 && strings[2].equalsIgnoreCase("true")) {
                        thunder = true;
                    }

                    Weather.lockWeather(plugin, ((Player) commandSender).getWorld(), true, strom, thunder);
                    commandSender.sendMessage("The weather circle is locked to Strom: " +
                            strom.toString() + " Thunder: " + thunder.toString());
                } else {
                    Weather.lockWeather(plugin, ((Player) commandSender).getWorld(), false,
                            ((Player) commandSender).getWorld().hasStorm(),
                            ((Player) commandSender).getWorld().isThundering());
                    commandSender.sendMessage("The weather circle is unlocked.");
                }


            }

        } else {
            commandSender.sendMessage(ConfigHelper.getString(plugin, "messages.error.onlyPlayers"));
        }

        return true;
    }
}
