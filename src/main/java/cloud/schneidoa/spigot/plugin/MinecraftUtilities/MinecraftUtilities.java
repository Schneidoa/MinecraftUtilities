package cloud.schneidoa.spigot.plugin.MinecraftUtilities;

import cloud.schneidoa.spigot.plugin.MinecraftUtilities.commands.TimeCommandExecutor;
import cloud.schneidoa.spigot.plugin.MinecraftUtilities.commands.WeatherCommandExecutor;
import cloud.schneidoa.spigot.plugin.MinecraftUtilities.world.Time;
import cloud.schneidoa.spigot.plugin.MinecraftUtilities.world.Weather;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public final class MinecraftUtilities extends JavaPlugin {
    @Override
    public void onEnable() {
        loadConfig();
        initPlugin();
        initCommands();
    }

    @Override
    public void onDisable() {
        // TODO Insert logic to be performed when the plugin is disabled
    }


    private void loadConfig() {

        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    private void initPlugin() {
        initTime();
        initWeather();

    }

    private void initTime() {
        List<World> worlds = Bukkit.getWorlds();
        for (World world : worlds) {
            if (getConfig().isSet(world.getName() + ".time.lock")
                    && getConfig().isSet(world.getName() + ".time.ticks")) {

                Time.lockTime(this, world, getConfig().getBoolean(world.getName() + ".time.lock"),
                        getConfig().getLong(world.getName() + ".time.ticks"));

            } else {
                getLogger().warning("Unable to load Property '" + world.getName() + ".time.lock"
                        + "' and/or " + world.getName() + ".time.ticks" + "'");
            }
        }
    }

    private void initWeather() {
        List<World> worlds = Bukkit.getWorlds();
        for (World world : worlds) {
            if (getConfig().isSet(world.getName() + ".weather.lock")
                    && getConfig().isSet(world.getName() + ".weather.storm")
                    && getConfig().isSet(world.getName() + ".weather.thunder")) {

                Weather.lockWeather(this, world, getConfig().getBoolean(world.getName() + ".weather.lock"),
                        getConfig().getBoolean(world.getName() + ".weather.storm"),
                        getConfig().getBoolean(world.getName() + ".weather.thunder"));

            } else {
                getLogger().warning("Unable to load Property '" + world.getName() + ".weather.lock"
                        + "' and/or " + world.getName() + ".weather.storm" + "'" +
                        " and/or " + world.getName() + ".weather.thunder");
            }
        }
    }

    private void initCommands() {
        this.getCommand("locktime").setExecutor(new TimeCommandExecutor(this));
        this.getCommand("lockweather").setExecutor(new WeatherCommandExecutor(this));
    }
}
