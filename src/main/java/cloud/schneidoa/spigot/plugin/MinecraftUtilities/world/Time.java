package cloud.schneidoa.spigot.plugin.MinecraftUtilities.world;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class Time {

    private static BukkitTask resetTimeTask;

    public static void setTime(World world, Long ticks) {
        world.setTime(ticks);
    }

    public static void lockTime(Plugin plugin, World world, boolean lock, Long ticks) {

        if (resetTimeTask != null) {
            resetTimeTask.cancel();
        }

        if (lock) {
            createResetTimeTask(plugin, world, ticks);
        }else{
            Time.setTime(world, ticks);
        }
    }

    private static void createResetTimeTask(Plugin plugin, World world, Long ticks) {
        Time.setTime(world, ticks);
        resetTimeTask = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> Time.setTime(world, ticks)
                , 20L, 20L);
    }
}
