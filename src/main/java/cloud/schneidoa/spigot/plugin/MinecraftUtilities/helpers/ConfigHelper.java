package cloud.schneidoa.spigot.plugin.MinecraftUtilities.helpers;

import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

public class ConfigHelper {

    public static String getString(Plugin plugin, String configPath) {

        return ChatColor.translateAlternateColorCodes('$', plugin.getConfig().getString(configPath));
    }
}
