package cloud.schneidoa.spigot.plugin.MinecraftUtilities.world;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class Weather {

    private static BukkitTask resetWeatherTask;

    public static void setWeather(World world, boolean storm, boolean thunder) {
        world.setStorm(storm);
        world.setThundering(thunder);
    }

    public static void lockWeather(Plugin plugin, World world, boolean lock, boolean storm, boolean thunder) {

        if (resetWeatherTask != null) {
            resetWeatherTask.cancel();
        }

        if (lock) {
            createResetWeatherTask(plugin, world, storm, thunder);
        }
    }

    private static void createResetWeatherTask(Plugin plugin, World world, boolean storm, boolean thunder) {
        Weather.setWeather(world, storm, thunder);
        resetWeatherTask = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () ->
                Weather.setWeather(world, storm, thunder), 100L, 100L);
    }
}
